# Paragraphs Frontend UI

DEMO: https://youtu.be/-jnzsOYhNp8

Paragraphs Frontend ui is a way of 
editing paragraphs from the frontend.

It uses contextual links to provide a number of editorial tasks in the frontend:

* Editing of the content inside a modal
* Edit 'Settings' inside settings tray
* Move paragraph items up/down
* Duplicate paragraph items
* Duplicate paragraph library items

## 8.2 Release

The 8.2 Release no longer uses Paragraph Sets, it allows duplicating Paragraph Library Items instead.

The paragraph from inside the Library Item is duplicated, so that it can be edited without altering the resuseable paragraph itself.
This makes it easy to setup pre-defined content blocks.

Paragraph Library Items added on the entity edit form can be edited from the frontend.
Since these items exist in another parent entity, it is not possible to add or duplicate items inside the library item.

## Install

1. Enable the module as usual

2. Edit the form mode of your paragraph types to have the Settings and Content form mode available.

## Known problems

Since we use contextual links for this, 
at least one paragraph has to exist on a node before the functionality can be used.
