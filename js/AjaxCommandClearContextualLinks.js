(function (Drupal) {

  /**
   * Not a perfect solution.
   * But rather a "hack/workaround" to make nested paragraphs work with this.
   * This will have some "additional" load ofcourse since it will reload some contextual links.
   *
   * @param ajax
   * @param response
   * @param status
   */
  Drupal.AjaxCommands.prototype.clearContextualLinks = function (ajax, response, status) {
    window.sessionStorage.clear();
  };

})( Drupal);
