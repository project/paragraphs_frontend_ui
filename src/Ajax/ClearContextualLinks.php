<?php

namespace Drupal\paragraphs_frontend_ui\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class ClearContextualLinks.
 */
class ClearContextualLinks implements CommandInterface {

  /**
   * Render custom ajax command.
   *
   * @return array RenderArray
   *   Command function.
   */
  public function render() {
    return [
      'command' => 'clearContextualLinks',
    ];
  }

}
