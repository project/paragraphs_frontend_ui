<?php

namespace Drupal\paragraphs_frontend_ui\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Render\Element;
use Drupal\Core\TypedData\TypedData;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs_edit\ParagraphLineageInspector;
use Drupal\paragraphs_frontend_ui\Ajax\ClearContextualLinks;
use Drupal\paragraphs_frontend_ui\Form\SettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for up and down actions.
 */
class ParagraphsFrontendUIController extends ControllerBase {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The Paragraph Lineage Inspector.
   *
   * @var \Drupal\paragraphs_edit\ParagraphLineageInspector
   */
  protected $paragraphLineageInspector;

  /**
   * Constructs a new QuickEditController.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface
   *   config factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   */
  public function __construct(ConfigFactory $configFactory, EntityTypeManager $entityTypeManager, FormBuilder $formBuilder, ParagraphLineageInspector $paragraphLineageInspector) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
    $this->paragraphLineageInspector = $paragraphLineageInspector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('paragraphs_edit.lineage.inspector')
    );
  }

  /**
   * Shift up a single paragraph.
   */
  public function up($paragraph, $js = 'nojs') {
    $paragraph = $paragraph->getTranslation($this->langcode());
    extract($this->getParentData($paragraph));

    $paragraph_items = $parent->$parent_field_name->getValue();
    foreach ($paragraph_items as $delta => $paragraph_item) {
      if ($paragraph_item['target_id'] == $paragraph->id()) {
        if ($delta > 0) {
          $prev_paragraph = $paragraph_items[$delta - 1];
          $paragraph_items[$delta - 1] = $paragraph_items[$delta];
          $paragraph_items[$delta] = $prev_paragraph;
        }
        break;
      }
    }
    $parent->$parent_field_name->setValue($paragraph_items);
    $parent->save();

    return $this->refreshWithAJaxResponse($parent, $parent_field_name);
  }

  /**
   * Shift down a single paragraph.
   */
  public function down($paragraph, $js = 'nojs') {
    $paragraph = $paragraph->getTranslation($this->langcode());
    extract($this->getParentData($paragraph));

    $paragraph_items = $parent->$parent_field_name->getValue();
    $numitems = count($paragraph_items);
    foreach ($paragraph_items as $delta => $paragraph_item) {
      if ($paragraph_item['target_id'] == $paragraph->id()) {
        if ($delta < $numitems) {
          $next_paragraph = $paragraph_items[$delta + 1];
          $paragraph_items[$delta + 1] = $paragraph_items[$delta];
          $paragraph_items[$delta] = $next_paragraph;
        }
        break;
      }
    }
    $parent->$parent_field_name->setValue($paragraph_items);
    $parent->save();

    return $this->refreshWithAJaxResponse($parent, $parent_field_name);
  }

  /**
   * Duplicate a paragraph.
   */
  public function duplicate($paragraph, $js = 'nojs') {
    $paragraph->getTranslation($this->langcode());
    extract($this->getParentData($paragraph));

    $paragraph_items = $parent->$parent_field_name->getValue();
    $paragraphs_new = [];
    foreach ($paragraph_items as $delta => $paragraph_item) {
      $paragraphs_new[] = $paragraph_item;
      if ($paragraph_item['target_id'] == $paragraph->id()) {

        $cloned_paragraph = $paragraph->createDuplicate();
        $cloned_paragraph->save();
        $paragraphs_new[] = [
          'target_id' => $cloned_paragraph->id(),
          'target_revision_id' => $cloned_paragraph->getRevisionId(),
        ];

      }
    }
    $parent->$parent_field_name->setValue($paragraphs_new);
    $parent->save();

    return $this->refreshWithAJaxResponse($parent, $parent_field_name);
  }

  /**
   * Select a paragraph type.
   */
  public function addBelow($paragraph, $js = 'nojs') {
    $form = \Drupal::formBuilder()->getForm('Drupal\paragraphs_frontend_ui\Form\ParagraphsFrontendUIAddBelow', $paragraph);
    return $form;
  }

  /**
   * Deny access for paragraph library items
   */
  private function parentIsLibraryItem($parent) {
    if ($parent->getEntityTypeId() == 'paragraphs_library_item') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if this is the first paragraph in an entity, if not, deny access.
   */
  public function accessUp($paragraph) {
    $paragraph = Paragraph::load($paragraph);
    extract($this->getParentData($paragraph));

    $paragraph_items = $parent->$parent_field_name->getValue();
    if ($paragraph_items[0]['target_id'] == $paragraph->id()) {
      return AccessResult::forbidden();
    }
    else {
      return AccessResult::allowed();
    }

  }

  /**
   * Check if this is the last paragraph in an entity, if not, deny access.
   */
  public function accessDown($paragraph) {
    $paragraph = Paragraph::load($paragraph);
    extract($this->getParentData($paragraph));

    $paragraph_items = $parent->$parent_field_name->getValue();
    $last_item = end($paragraph_items);
    if ($last_item['target_id'] == $paragraph->id()) {
      return AccessResult::forbidden();
    }
    else {
      return AccessResult::allowed();
    }

  }

  /**
   * Check if the settings form mode exists for this paragraph.
   */
  public function accessLibraryItem($paragraph) {
    $paragraph = Paragraph::load($paragraph);
    extract($this->getParentData($paragraph));

    if ($this->parentIsLibraryItem($parent)) {
      return AccessResult::forbidden();
    }
    else {
      return AccessResult::allowed();
    }
  }

  /**
   * Check if the settings form mode exists for this paragraph.
   */
  public function accessEditSettings($paragraph) {
    return $this->accessFormMode($paragraph, 'settings');
  }

  /**
   * Check if the settings form mode exists for this paragraph.
   */
  public function accessEditContent($paragraph) {
    return $this->accessFormMode($paragraph, 'content');
  }

  /**
   * Check if the form mode exists for this paragraph.
   */
  public function accessFormMode($paragraph, $form_mode) {
    $paragraph = Paragraph::load($paragraph);
    $display_config_id = 'core.entity_form_display.paragraph.' . $paragraph->bundle() . '.' . $form_mode;
    if (\Drupal::configFactory()->loadMultiple([$display_config_id])) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden();
    }
  }

  /**
   * Helper function to get the required data about the parent of the paragraph.
   */
  private function getParentData($paragraph) {
    $parent = $paragraph->getParentEntity()->getTranslation($this->langcode());
    return [
      'parent' => $parent,
      'parent_type' => $parent->getEntityTypeId(),
      'parent_bundle' => $parent->bundle(),
      'parent_entity_id' => $parent->id(),
      'parent_field_name' => $paragraph->get('parent_field_name')->getValue()[0]['value'],
    ];
  }

  /**
   * Helper function to refresh the field with ajax.
   */
  private function refreshWithAJaxResponse($entity, $field_name) {
    $identifier = '[data-paragraphs-frontend-ui=' . $field_name . '-' . $entity->id() . ']';
    $field = $entity->get($field_name);
    $this->forceValueLanguage($field, $this->langcode());
    $response = new AjaxResponse();

    if ($this->configFactory->get(SettingsForm::SETTINGS_NAME)->get('enable_nested_paragraphs')) {
      $response->addCommand(
        new ClearContextualLinks()
      );
    }
    // Refresh the paragraphs field.
    $response->addCommand(
      new ReplaceCommand(
        $identifier,
        $field->view('default')
      )
    );

    return $response;
  }

  /**
   * Helper function to get the current langcode
   */
  private function langcode() {
    return $this->languageManager()->getCurrentLanguage()->getId();
  }

  /**
   * After reloading a translated paragraph field with ajax,
   * the original language is shown instead of the translation
   * I am using the workaround provided in
   * https://www.drupal.org/project/paragraphs/issues/2753201#comment-11834096
   * to force the language
   *
   * @param \Drupal\Core\TypedData\TypedData $value
   * @param $language
   */
  function forceValueLanguage(TypedData &$value,$language) {
    $parent=$value->getParent()->getValue();
    if (!$parent->hasTranslation($language)) return;
    $parent_translated=$parent->getTranslation($language);
    $name=$value->getName();
    $adapter=EntityAdapter::createFromEntity($parent_translated);
    $value->setContext($name,$adapter);
  }
}
