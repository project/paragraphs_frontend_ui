<?php

namespace Drupal\paragraphs_frontend_ui\Form;

use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\paragraphs_frontend_ui\Entity\ParagraphSet;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\paragraphs_library\Entity\LibraryItem;

/**
 * Class CleanupUrlAliases.
 *
 * @package Drupal\paragraphs_ui_add_set\Form
 */
class ParagraphsFrontendUIAddBelow extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_frontend_ui_add_set';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $paragraph = NULL) {

    // Set the paragraph to the form state.
    $form_state->addBuildInfo('paragraph', $paragraph);

    // Render the sets.
    $sets = \Drupal::entityTypeManager()->getListBuilder('paragraphs_library_item')->load();


    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('paragraphs_library_item');
    if (empty($sets)) {
      $form['empty_text'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No reusable paragraphs found.'),
      ];
    }
    $form['sets']['#attached']['library'][] = 'paragraphs_frontend_ui/paragraphs_frontend_ui.theme';
    foreach ($sets as $set) {
      $form['sets'][$set->id()] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['add-paragraph-item'],
        ]
      ];
      $form['sets'][$set->id()]['element'] = $view_builder->view($set);
      $form['sets'][$set->id()]['add_set'] = [
        '#type' => 'button',
        '#name' => $set->id() . '_add_set',
        '#value' => $this->t('Add'),
        '#ajax' => [
          'callback' => [$this, 'addMoreAjax'],
          'effect' => 'fade',
        ],
      ];
    }

    $form['sets']['clearfix'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['clearfix'],
      ]
    ];

    $user = \Drupal::currentUser();
    $form['add_set'] = [
      '#type' => 'link',
      '#title' => $this->t('Add a new item'),
      '#url' => Url::fromRoute('entity.paragraphs_library_item.add_form', [
        'destination' => \Drupal::destination()->get(),
      ]),
      '#access' => $user->hasPermission('create paragraph library item'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo create an ajax fallback
  }

  /**
   * {@inheritdoc}
   */
  public function addMoreAjax(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();

    $triggering_paragraph = $build_info['paragraph'];

    $trigger = $form_state->getTriggeringElement()['#name'];
    $set = substr($trigger, 0, -8);
    if (is_numeric($set)) {

      $set = LibraryItem::load($set);
      $target_id = $set->get('paragraphs')->getValue()[0]['target_id'];
      $set_paragraph = Paragraph::load($target_id);

      $parent = $triggering_paragraph->getParentEntity();

      $parent_type = $parent->getEntityTypeId();
      $parent_bundle = $parent->getType();
      $parent_entity_id = $parent->id();
      $parent_field_name = $triggering_paragraph->get('parent_field_name')->getValue()[0]['value'];

      $paragraph_items = $parent->$parent_field_name->getValue();
      $paragraphs_new = [];
      foreach ($paragraph_items as $delta => $paragraph_item) {
        $paragraphs_new[] = $paragraph_item;
        if ($paragraph_item['target_id'] == $triggering_paragraph->id()) {

          $new_paragraph = $set_paragraph->createDuplicate();
          // set the language off the parent
          $langcode = \Drupal::service('language_manager')->getCurrentLanguage()->getId();
          $new_paragraph->set('langcode', $langcode);

          $new_paragraph->save();
          $paragraphs_new[] = [
            'target_id' => $new_paragraph->id(),
            'target_revision_id' => $new_paragraph->getRevisionId(),
          ];
        }

      }
      $parent->$parent_field_name->setValue($paragraphs_new);
      $parent->save();

      $identifier = '[data-paragraphs-frontend-ui=' . $parent_field_name . '-' . $parent->id() . ']';
      $response = new AjaxResponse();
      // Refresh the paragraphs field.
      $response->addCommand(
        new ReplaceCommand(
          $identifier,
          $parent->get($parent_field_name)->view('default')
        )
      );
      $response->addCommand(new CloseModalDialogCommand());
      return $response;

    }

  }


}
