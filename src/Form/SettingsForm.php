<?php

namespace Drupal\paragraphs_frontend_ui\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  public const SETTINGS_NAME = 'paragraphs_frontend_ui.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_frontend_ui';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGS_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * This form should reflect the fields in the zone content type from the
   * police_zones module. Settings will be stored localy in a config storage,
   * but will also be synched to the central storage.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_NAME);

    $form['form_modes'] = [
      '#type' => 'details',
      '#title' => $this->t('Form modes'),
    ];
    $form['form_modes']['pfui_edit_content_form_mode'] =  [
      '#type' => 'select',
      '#title' => $this->t('Form mode for editing content'),
      '#options' => $this->getFormModesForType('paragraph'),
      '#default_value' => $config->get('pfui_edit_content_form_mode'),
    ];

    $form['dialog_style'] = [
      '#type' => 'details',
      '#title' => $this->t('Dialog settings'),
      '#description' => $this->t('Contextual links are aggressively cached. To make changes to these field work, 
                                      you will need to edit the parent node so that the cache is completely rebuild. 
                                      Only clearing cache will not trigger the changes.'),
    ];
    $form['dialog_style']['pfui_edit_content_dialog'] = [
      '#type' => 'select',
      '#title' => $this->t('Dialog type for editing content'),
      '#options' => [
        'modal' => $this->t('Modal'),
        'off_canvas' => $this->t('Settings tray'),
      ],
      '#default_value' => $config->get('pfui_edit_content_dialog'),
    ];
    $form['dialog_style']['pfui_edit_settings_dialog'] = [
      '#type' => 'select',
      '#title' => $this->t('Dialog type for editing settings'),
      '#options' => [
        'modal' => $this->t('Modal'),
        'off_canvas' => $this->t('Settings tray'),
      ],
      '#default_value' => $config->get('pfui_edit_settings_dialog'),
    ];
    $form['dialog_style']['pfui_add_below_dialog'] = [
      '#type' => 'select',
      '#title' => $this->t('Dialog type for adding paragraphs'),
      '#options' => [
        'modal' => $this->t('Modal'),
        'off_canvas' => $this->t('Settings tray'),
      ],
      '#default_value' => $config->get('pfui_add_below_dialog'),
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
    ];
    $form['advanced']['enable_nested_paragraphs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable nested paragraphs fix'),
      '#default_value' => $config->get('enable_nested_paragraphs'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(self::SETTINGS_NAME)
      ->set('enable_nested_paragraphs', $form_state->getValue('enable_nested_paragraphs'))
      ->set('pfui_edit_content_dialog', $form_state->getValue('pfui_edit_content_dialog'))
      ->set('pfui_add_below_dialog', $form_state->getValue('pfui_add_below_dialog'))
      ->set('pfui_edit_settings_dialog', $form_state->getValue('pfui_edit_settings_dialog'))
      ->set('pfui_edit_content_form_mode', $form_state->getValue('pfui_edit_content_form_mode'))
      ->save();
  }

  protected function getFormModesForType($entity_type) {
    $form_modes = \Drupal::service('entity_display.repository')->getFormModes($entity_type);
    $options = [];
    foreach ($form_modes as $id => $form_mode) {
      $options["$entity_type.$id"] = $id;
    }
    return $options;
  }

}
