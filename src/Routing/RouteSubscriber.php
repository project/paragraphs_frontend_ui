<?php

namespace Drupal\paragraphs_frontend_ui\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\paragraphs_frontend_ui\Form\SettingsForm;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('paragraphs_edit.edit_form')) {
      $config = \Drupal::config(SettingsForm::SETTINGS_NAME);
      $route->addDefaults(['_entity_form' => $config->get('pfui_edit_content_form_mode')]);
      $route->addRequirements(['_custom_access' => '\Drupal\paragraphs_frontend_ui\Controller\ParagraphsFrontendUIController::accessEditContent']);
    }
    if ($route = $collection->get('paragraphs_edit.delete_form')) {
      $route->addRequirements(['_custom_access' => '\Drupal\paragraphs_frontend_ui\Controller\ParagraphsFrontendUIController::accessLibraryItem']);
    }
  }

}
